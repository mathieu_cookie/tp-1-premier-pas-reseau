TP5 : Self-hosted cloud

Partie 1 : Mise en place et maîtrise du serveur Web

1. Installation
🌞 Installer le serveur Apache

[mathieu@web ~]$ sudo dnf install -y httpd


🌞 Démarrer le service Apache

[mathieu@web ~]$ sudo systemctl start httpd
[mathieu@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor pre>
   Active: active (running) since Tue 2023-01-17 11:12:24 CET; 31s ago
     Docs: man:httpd.service(8)
 Main PID: 2078 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 4916)
   Memory: 29.7M
   CGroup: /system.slice/httpd.service
           ├─2078 /usr/sbin/httpd -DFOREGROUND
           ├─2079 /usr/sbin/httpd -DFOREGROUND
           ├─2080 /usr/sbin/httpd -DFOREGROUND
           ├─2081 /usr/sbin/httpd -DFOREGROUND
           └─2082 /usr/sbin/httpd -DFOREGROUND
[mathieu@web ~]$ sudo ss -alpnt | grep httpd
LISTEN 0      128                *:80              *:*    users:(("httpd",pid=2082,fd=4),("httpd",pid=2081,fd=4),("httpd",pid=2080,fd=4),("httpd",pid=2078,fd=4))
[mathieu@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.



[mathieu@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[mathieu@web ~]$ sudo firewall-cmd --reload
success
[mathieu@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:


🌞 TEST

[mathieu@web ~]$ sudo systemctl is-active httpd
active
[mathieu@web ~]$ sudo systemctl is-enabled httpd
enabled
[mathieu@web ~]$ curl localhost | head
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
$ curl 10.105.1.11 | head
doctype html>
tml>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {




2. Avancer vers la maîtrise du service
🌞 Le service Apache...

[mathieu@web ~]$ cat /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target


🌞 Déterminer sous quel utilisateur tourne le processus Apache

[mathieu@web ~]$ cat /etc/httpd/conf/httpd.conf | grep apache
User apache
Group apache
[mathieu@web ~]$ ps -ef | grep apache
apache       802     779  0 15:11 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       804     779  0 15:11 ?        00:00:03 /usr/sbin/httpd -DFOREGROUND
apache       805     779  0 15:11 ?        00:00:03 /usr/sbin/httpd -DFOREGROUND
apache       806     779  0 15:11 ?        00:00:03 /usr/sbin/httpd -DFOREGROUND
mathieu+    1708    1648  0 15:38 pts/0    00:00:00 grep --color=auto apache
[mathieu@web ~]$ ls -al /usr/share/testpage/
total 12
drwxr-xr-x.  2 root root   24 Jan 17 11:05 .
drwxr-xr-x. 91 root root 4096 Jan 17 11:05 ..
-rw-r--r--.  1 root root 7620 Jul 27 20:04 index.html


🌞 Changer l'utilisateur utilisé par Apache

[mathieu@web ~]$ sudo useradd mathieu -d /usr/share/httpd/ -s /sbin/nologin -u 2000
[mathieu@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
[mathieu@web ~]$ cat /etc/httpd/conf/httpd.conf | grep mathieu
User mathieu
[mathieu@web ~]$ ps -ef | grep mathieu
root         791       1  0 15:11 ?        00:00:00 login -- mathieu
mathieu+    1601       1  0 15:17 ?        00:00:00 /usr/lib/systemd/systemd --user
mathieu+    1604    1601  0 15:17 ?        00:00:00 (sd-pam)
mathieu+    1611     791  0 15:17 tty1     00:00:00 -bash
root        1643     784  0 15:17 ?        00:00:00 sshd: mathieu [priv]
mathieu+    1647    1643  0 15:17 ?        00:00:00 sshd: mathieu@pts/0
mathieu+    1648    1647  0 15:17 pts/0    00:00:00 -bash
mathieu       1761    1758  0 16:02 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
mathieu       1762    1758  0 16:02 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
mathieu       1763    1758  0 16:02 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
mathieu       1764    1758  0 16:02 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
mathieu+    1994    1648  0 16:09 pts/0    00:00:00 ps -ef
mathieu+    1995    1648  0 16:09 pts/0    00:00:00 grep --color=auto mathieu


🌞 Faites en sorte que Apache tourne sur un autre port

[mathieu@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
[mathieu@web ~]$ cat /etc/httpd/conf/httpd.conf | grep 045
Listen 045
[mathieu@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[mathieu@web ~]$ sudo firewall-cmd --add-port=045/tcp --permanent
success
[mathieu@web ~]$ sudo firewall-cmd --reload
success
[mathieu@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 45/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[mathieu@web ~]$ sudo systemctl restart httpd
[mathieu@web ~]$ sudo ss -altpn | grep httpd
LISTEN 0      128                *:45              *:*    users:(("httpd",pid=2042,fd=4),("httpd",pid=2041,fd=4),("httpd",pid=2040,fd=4),("httpd",pid=2036,fd=4))
[mathieu@web ~]$ curl 10.105.1.11:045 | head
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7620  100  7620    0     0   826k      0 --:--:-- --:--:-- --:--:-- 1240k
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {


📁 Fichier /etc/httpd/conf/httpd.conf

Partie 2 : Mise en place et maîtrise du serveur de base de données
🌞 Install de MariaDB sur db.tp5.linux

[mathieu@db ~]$ sudo dnf install mariadb-server
[mathieu@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
[mathieu@db ~]$ sudo systemctl start mariadb
[mathieu@db ~]$ sudo mysql_secure_installation


🌞 Port utilisé par MariaDB

[mathieu@db ~]$ sudo ss -alpnt | grep 80
LISTEN 0      80                 *:3306            *:*    users:(("mysqld",pid=26321,fd=21))



[mathieu@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[mathieu@db ~]$ sudo firewall-cmd --reload
success
[mathieu@db ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 3306/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:


🌞 Processus liés à MariaDB

[mathieu@db ~]$ ps -ef | grep mysqld | grep usr
mysql      26321       1  0 09:49 ?        00:00:01 /usr/libexec/mysqld --basedir=/usr



Partie 3 : Configuration et mise en place de NextCloud

1.Base de données
🌞 Préparation de la base pour NextCloud

[mathieu@db ~]$ sudo mysql -u root -p
[sudo] password for mathieu:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 16
Server version: 10.3.35-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.105.1.11' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.105.1.11';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)


🌞 Exploration de la base de données
///

[mathieu@web ~]$ sudo dnf install mysql -y


///

[mathieu@web ~]$ mysql -u nextcloud -h 10.105.1.12 -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 25
Server version: 5.5.5-10.3.35-MariaDB MariaDB Server

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>



mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)

mysql> USE nextcloud;
Database changed
mysql>  show tables;
Empty set (0.00 sec)


🌞 Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données

[mathieu@db ~]$ mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 26
Server version: 10.3.35-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SELECT User FROM mysql.user;
+-----------+
| User      |
+-----------+
| nextcloud |
| root      |
| root      |
| root      |
+-----------+
4 rows in set (0.001 sec)

MariaDB [(none)]>



2.Serveur Web et NextCloud
[mathieu@web conf]$ sudo dnf config-manager --set-enabled crb
[mathieu@web conf]$ sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
[mathieu@web conf]$ dnf module list php
[mathieu@web conf]$ sudo dnf module enable php:remi-8.1 -y
[mathieu@web conf]$ sudo dnf install -y php81-php
[mathieu@web conf]$ sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
[mathieu@web www]$ sudo curl https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip -O
[mathieu@web www]$ ls /var/www/tp5_nextcloud/
3rdparty  config       core      index.html  occ           ocs-provider  resources   themes
apps      console.php  cron.php  index.php   ocm-provider  public.php    mathieus.txt  updater
AUTHORS   COPYING      dist      lib         ocs           remote.php    status.php  version.php
[mathieu@web tp5_nextcloud]$ sudo chown apache:apache /var/www/tp5_nextcloud/ -R
[mathieu@web conf]$ sudo cat httpd.conf | tail -n 18
IncludeOptional conf.d/*.conf

<VirtualHost *:80>
  # on indique le chemin de notre webroot
  DocumentRoot /var/www/tp5_nextcloud/
  # on précise le nom que saisissent les clients pour accéder au service
  ServerName  web.tp5.linux

  # on définit des règles d'accès sur notre webroot
  <Directory /var/www/tp5_nextcloud/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
[mathieu@web conf]$ sudo systemctl restart httpd


3.Finaliser l'installation de NextCloud
🌞 Exploration de la base de données

[mathieu@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 8
Server version: 10.3.35-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> select count(*) as NumberOfTableInNextCloud from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'ne
    '> xtcloud';
+--------------------------+
| NumberOfTableInNextCloud |
+--------------------------+
|                        0 |
+--------------------------+
1 row in set (0.004 sec)

MariaDB [(none)]> select count(*) from information_schema.tables where table_type = 'BASE TABLE';
+----------+
| count(*) |
+----------+
|       83 |
+----------+
1 row in set (0.029 sec)