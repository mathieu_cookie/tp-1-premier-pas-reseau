TP2 : Appréhender l'environnement Linux

I. Service SSH

1. Analyse du service
🌞 S'assurer que le service sshd est démarré

[mathieu@localhost ~]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor pr>
   Active: active (running) since Fri 2022-12-09 16:59:31 CET; 14min ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 775 (sshd)
    Tasks: 1 (limit: 4916)
   Memory: 3.9M
   CGroup: /system.slice/sshd.service
           └─775 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha2>


🌞 Analyser les processus liés au service SSH

afficher les processus liés au service sshd

vous pouvez afficher la liste des processus en cours d'exécution avec une commande ps

[mathieu@localhost ~]$ ps
    PID TTY          TIME CMD
   1421 pts/0    00:00:00 bash
   1525 pts/0    00:00:00 ps

pour le compte-rendu, vous devez filtrer la sortie de la commande en ajoutant | grep <TEXTE_RECHERCHE>

[mathieu@localhost ~]$ ps -ef|grep sshd
root         782       1  0 10:53 ?        00:00:00 /usr/sbin/sshd -D  
root        1399     782  0 10:54 ?        00:00:00 sshd: mathieu [priv]
robot-0+    1403    1399  0 10:54 ?        00:00:02 sshd: mathieu@pts/0
robot-0+    1602    1404  0 11:21 pts/0    00:00:00 grep --color=auto sshd

🌞 Déterminer le port sur lequel écoute le service SSH

[mathieu@localhost ~]$ ss | grep ssh
[mathieu@localhost ~]$ ss | grep ssh
tcp   ESTAB 0      36                                             192.168.199.2:ssh   192.168.199.1:52339

Le port d'écoute est le 52339
🌞 Consulter les logs du service SSH

[mathieu@localhost ~]$ journalctl
-- Logs begin at Sun 2022-12-11 10:53:32 CET, end at Sun 2022-12-11 10>
Dec 11 10:53:32 localhost.localdomain kernel: Linux version 4.18.0-372>
Dec 11 10:53:32 localhost.localdomain kernel: Command line: BOOT_IMAGE>
...

connection au log avec la commande suivante

[mathieu@localhost ~]$ cd /var/log

tail des 10 dernière lignes de ce fichier 

[mathieu@localhost log]$ journalctl | grep ssh | tail -n 10
Dec 11 10:53:41 localhost.localdomain systemd[1]: Reached target sshd-keygen.target.
Dec 11 10:53:43 localhost.localdomain sshd[782]: Server listening on 0.0.0.0 port 22.
Dec 11 10:53:43 localhost.localdomain sshd[782]: Server listening on :: port 22.
Dec 11 10:54:47 localhost.localdomain sshd[1399]: Accepted password for mathieu from 192.168.199.1 port 52339 ssh2
Dec 11 10:54:47 localhost.localdomain sshd[1399]: pam_unix(sshd:session): session opened for user mathieu by (uid=0)

2. Modification du service
🌞 Identifier le fichier de configuration du serveur SSH

[mathieu@localhost ~]$ cd /etc/ssh/
[mathieu@localhost ssh]$ ls
ssh_config

🌞 Modifier le fichier de conf

[mathieu@localhost ssh]$ echo $RANDOM
4049

[mathieu@localhost ssh]$ sudo cat sshd_config | grep Port
Port 4049

[mathieu@localhost ssh]$ sudo firewall-cmd --remove-service=ssh --permanent
success
[mathieu@localhost ssh]$ sudo firewall-cmd --add-port=4049/tcp --permanent
success
[mathieu@localhost ssh]$ sudo firewall-cmd --reload
  ports: 4049/tcp
[mathieu@localhost ssh]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s8
  sources:
  services: cockpit dhcpv6-client
  ports: 4049/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

🌞 Redémarrer le service

[mathieu@localhost ssh]$  sudo systemctl restart sshd

🌞 Effectuer une connexion SSH sur le nouveau port

PS C:\Users\mathi> ssh mathieu@192.168.199.2 -p 4049
mathieu@192.168.199.2's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Dec 14 11:13:19 2022 from 192.168.199.1

II. Service HTTP

1. Mise en place
🌞 Installer le serveur NGINX

[mathieu@localhost ~]$ sudo dnf install nginx
[sudo] password for mathieu:
Rocky Linux 8 - AppStream              3.1 kB/s | 4.7 kB     00:01
Rocky Linux 8 - AppStream              509 kB/s | 8.6 MB     00:17
Rocky Linux 8 - BaseOS                  14 kB/s | 4.3 kB     00:00
Rocky Linux 8 - BaseOS                 3.2 MB/s | 2.7 MB     00:00
Rocky Linux 8 - Extras                  11 kB/s | 3.5 kB     00:00
Rocky Linux 8 - Extras                 5.6 kB/s |  11 kB     00:02
Dependencies resolved.
=======================================================================
 Package                     Arch   Version            Repo       Size
=======================================================================
Installing:
 nginx                       x86_64 1:1.14.1-9.module+el8.4.0+542+81547229
                                                       appstream 566 k
Installing dependencies:
 fontconfig                  x86_64 2.13.1-4.el8       baseos    273 k
 gd                          x86_64 2.2.5-7.el8        appstream 143 k
 jbigkit-libs                x86_64 2.1-14.el8         appstream  54 k
 libX11                      x86_64 1.6.8-5.el8        appstream 610 k
 
Transaction Summary
=======================================================================
Install  61 Packages

Complete!

🌞 Démarrer le service NGINX

[mathieu@localhost ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.

🌞 Déterminer sur quel port tourne NGINX

[mathieu@localhost ~]$ sudo ss -alnpt | grep nginx
[sudo] password for mathieu:
LISTEN 0      128          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=26079,fd=8),("nginx",pid=26078,fd=8),("nginx",pid=26077,fd=8))
LISTEN 0      128             [::]:80           [::]:*    users:(("nginx",pid=26079,fd=9),("nginx",pid=26078,fd=9),("nginx",pid=26077,fd=9))


[mathieu@localhost ~]$ sudo firewall-cmd --permanent --add-service=http
success
[mathieu@localhost ~]$ sudo firewall-cmd --permanent --list-all
public
  target: default
  icmp-block-inversion: no
  interfaces:
  sources:
  services: cockpit dhcpv6-client http
  ports: 4049/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[mathieu@localhost ~]$ sudo firewall-cmd --reload
success

🌞 Déterminer les processus liés à l'exécution de NGINX

[mathieu@localhost ~]$ ps -ef | grep nginx
root       26077       1  0 12:04 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx      26078   26077  0 12:04 ?        00:00:00 nginx: worker process
nginx      26079   26077  0 12:04 ?        00:00:00 nginx: worker process
robot-0+   26158   26099  0 12:10 pts/0    00:00:00 grep --color=auto nginx

🌞 Euh wait

[mathieu@localhost ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2022-12-14 12:04:43 CET; 1h 41min ago
  Process: 26076 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 26074 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 26072 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 26077 (nginx)
    Tasks: 3 (limit: 4916)
   Memory: 5.0M
   CGroup: /system.slice/nginx.service
           ├─26077 nginx: master process /usr/sbin/nginx
           ├─26078 nginx: worker process
           └─26079 nginx: worker process

Dec 14 12:04:43 localhost.localdomain systemd[1]: Starting The nginx HTTP and reverse proxy server...
Dec 14 12:04:43 localhost.localdomain nginx[26074]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Dec 14 12:04:43 localhost.localdomain nginx[26074]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Dec 14 12:04:43 localhost.localdomain systemd[1]: nginx.service: Failed to parse PID from file /run/nginx.pid: Invalid argument
Dec 14 12:04:43 localhost.localdomain systemd[1]: Started The nginx HTTP and reverse proxy server.


[mathieu@localhost ~]$  curl 192.168.199.2 | head -n 7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --100  3429  100  3429    0     0  3348k      0 --:--:-- --:--:-- --:--:-- 3348k
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">

2. Analyser la conf de NGINX
🌞 Déterminer le path du fichier de configuration de NGINX

[mathieu@localhost ~]$ ls -al /etc/nginx/nginx.conf
-rw-r--r--. 1 root root 2469 Jun 10  2021 /etc/nginx/nginx.conf


🌞 Trouver dans le fichier de conf

les lignes qui permettent de faire tourner un site web d'accueil (la page moche que vous avez vu avec votre navigateur)

[mathieu@localhost nginx]$ cat nginx.conf | grep server -A 16
    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }

une ligne qui parle d'inclure d'autres fichiers de conf

encore un cat <FICHIER> | grep <TEXTE>

bah ouais, on stocke pas toute la conf dans un seul fichier, sinon ça serait le bordel

[mathieu@localhost nginx]$ cat nginx.conf | grep conf
# For more information on configuration, see:
include /usr/share/nginx/modules/*.conf;
    # Load modular configuration files from the /etc/nginx/conf.d directory.
    include /etc/nginx/conf.d/*.conf;
        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;
#        # Load configuration files for the default server block.
#        include /etc/nginx/default.d/*.conf;

3. Déployer un nouveau site web
🌞 Créer un site web

bon on est pas en cours de design ici, alors on va faire simplissime
créer un sous-dossier dans /var/www/

par convention, on stocke les sites web dans /var/www/

votre dossier doit porter le nom tp2_linux

dans ce dossier /var/www/tp2_linux, créez un fichier index.html

il doit contenir <h1>MEOW mon premier serveur web</h1>

🌞 Adapter la conf NGINX

dans le fichier de conf principal

vous supprimerez le bloc server {} repéré plus tôt pour que NGINX ne serve plus le site par défaut
redémarrez NGINX pour que les changements prennent effet

créez un nouveau fichier de conf

il doit être nommé correctement
il doit être placé dans le bon dossier
c'est quoi un "nom correct" et "le bon dossier" ?

bah vous avez repéré dans la partie d'avant les fichiers qui sont inclus par le fichier de conf principal non ?
créez votre fichier en conséquence

redémarrez NGINX pour que les changements prennent effet
le contenu doit être le suivant :

server {
  # le port choisi devra être obtenu avec un 'echo $RANDOM' là encore
  listen <PORT>;

  root /var/www/tp2_linux;
}

🌞 Visitez votre super site web

toujours avec une commande curl depuis votre PC (ou un navigateur)

III. Your own services

1. Au cas où vous auriez oublié
➜ Dans la VM

nc -l 8888

lance netcat en mode listen
il écoute sur le port 8888
sans rien préciser de plus, c'est le port 8888 TCP qui est utilisé

➜ Allumez une autre VM vite fait

nc <IP_PREMIERE_VM> 8888
vérifiez que vous pouvez envoyer des messages dans les deux sens

2. Analyse des services existants
🌞 Afficher le fichier de service SSH

vous pouvez obtenir son chemin avec un systemctl status <SERVICE>

mettez en évidence la ligne qui commence par ExecStart=

encore un cat <FICHIER> | grep <TEXTE>

c'est la ligne qui définit la commande lancée lorsqu'on "start" le service

taper systemctl start <SERVICE> ou exécuter cette commande à la main, c'est (presque) pareil

🌞 Afficher le fichier de service NGINX

mettez en évidence la ligne qui commence par ExecStart=

3. Création de service
🌞 Créez le fichier /etc/systemd/system/tp2_nc.service

son contenu doit être le suivant (nice & easy)

[Unit]
Description=Super netcat tout fou

[Service]
ExecStart=/usr/bin/nc -l <PORT>

🌞 Indiquer au système qu'on a modifié les fichiers de service

la commande c'est sudo systemctl daemon-reload

🌞 Démarrer notre service de ouf

avec une commande systemctl start