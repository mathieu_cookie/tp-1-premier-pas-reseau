# TP4 : TCP, UDP et services réseau

# [](#i-first-steps)I. First steps

🌞 **Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP**

rocket league in game :

-   IP dst = 54.229.54.165
-   Port dst = :226
-   Port src = :61301

```
PS C:\Windows\system32> netstat -b -n

[rocketleague.exe]
  TCP    10.33.16.166:52831     54.229.16.187:443      ESTABLISHED
```

Steam :

-   IP dst = 142.250.178.142
-   Port dst = :443
-   Port src = :61357

```
PS C:\Windows\system32> netstat -b -n

[steamwebhelper.exe]
  TCP    10.33.16.166:61357     142.250.178.142:443    TIME_WAIT
```

Discord :

-   IP dst = 74.125.133.94
-   Port dst = :443
-   Port src = :61311

```
PS C:\Windows\system32> netstat -b -n

[Discord.exe]
  TCP    10.33.16.166:61311     74.125.133.94:443      CLOSE_WAIT
```

Teams :

-   IP dst = 52.113.194.132
-   Port dst = :443
-   Port src = :61431

```
PS C:\Windows\system32> netstat -b -n

[Teams.exe]
  TCP    10.33.16.166:61431     52.113.194.132:443     ESTABLISHED
```

🦈🦈🦈🦈🦈 Une capture pour chaque application, qui met bien en évidence le trafic en question.

# [](#ii-mise-en-place)II. Mise en place

## [](#1-ssh)1. SSH

🖥️ **Machine `node1.tp4.b1`**

🌞 **Examinez le trafic dans Wireshark**

```
tp4_ssh.pcapng  🦈 
```

🌞 **Demandez aux OS**

-   repérez, avec une commande adaptée (`netstat` ou `ss`), la connexion SSH depuis votre machine

```
PS C:\Windows\system32> netstat -p TCP -n -b
Connexions actives
  Proto  Adresse locale         Adresse distante       État
  TCP    10.4.1.1:49717         10.4.1.11:22           ESTABLISHED
 [ssh.exe]
```

-   ET repérez la connexion SSH depuis votre VM

```
[mathieu@node1 ~]$ ss
Netid              State               Recv-Q               Send-Q                                           Local Address:Port                              Peer Address:Port               Process
tcp                ESTAB               0                    0                                                    10.4.1.11:ssh                                   10.4.1.1:63264
```

## [](#2-routage)2. Routage

🖥️ **Machine `router.tp4.b1`**

# [](#iii-dns)III. DNS

## [](#2-setup)2. Setup

🖥️ **Machine `dns-server.tp4.b1`**

🌞 **Dans le rendu, je veux**

-   un `cat` des fichiers de conf

**Fichier de conf principal**

```
[mathieu@dns-serveur ~]$ sudo cat /etc/named.conf
//
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//
options {
        listen-on port 53 { 127.0.0.1; any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { localhost; any; };
        allow-query-cache { localhost; any; };
        /*
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable
           recursion.
         - If your recursive DNS server has a public IP address, you MUST enable access
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface
        */
        recursion yes;
        dnssec-validation yes;
        managed-keys-directory "/var/named/dynamic";
        geoip-directory "/usr/share/GeoIP";
        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";
        /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
        include "/etc/crypto-policies/back-ends/bind.config";
};
logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};
# référence vers notre fichier de zone
zone "tp4.b1" IN {
     type master;
     file "tp4.b1.db";
     allow-update { none; };
     allow-query {any; };
};
# référence vers notre fichier de zone inverse
zone "1.4.10.in-addr.arpa" IN {
     type master;
     file "tp4.b1.rev";
     allow-update { none; };
     allow-query { any; };
};
zone "." IN {
        type hint;
        file "named.ca";
};
include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```

**Et pour les fichiers de zone**

```
# Fichier de zone pour nom -> IP
$ sudo cat /var/named/tp4.b1.db
$TTL 86400
@ IN SOA dns-server.tp4.b1. admin.tp4.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)
; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns-server.tp4.b1.
; Enregistrements DNS pour faire correspondre des noms à des IPs
dns-server IN A 10.4.1.201
node1      IN A 10.4.1.11
```

```
# Fichier de zone inverse pour IP -> nom
$ sudo cat /var/named/tp4.b1.rev
$TTL 86400
@ IN SOA dns-server.tp4.b1. admin.tp4.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)
; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns-server.tp4.b1.
;Reverse lookup for Name Server
201 IN PTR dns-server.tp4.b1.
11 IN PTR node1.tp4.b1.
```

-   un `systemctl status named` qui prouve que le service tourne bien

```
[mathieu@dns-serveur etc]$ systemctl status named
● named.service - Berkeley Internet Name Domain (DNS)
     Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; vendor>
     Active: active (running) since Tue 2022-11-01 20:51:01 CET; 48min ago
   Main PID: 704 (named)
      Tasks: 5 (limit: 2684)
     Memory: 29.6M
        CPU: 216ms
     CGroup: /system.slice/named.service
             └─704 /usr/sbin/named -u named -c /etc/named.conf
```

-   une commande `ss` qui prouve que le service écoute bien sur un port

```
[mathieu@dns-serveur etc]$ ss -lnt
State             Recv-Q            Send-Q                       Local Address:Port                         Peer Address:Port            Process
LISTEN            0                 10                              10.4.1.201:53                                0.0.0.0:*
```

🌞 **Ouvrez le bon port dans le firewall**

-   grâce à la commande `ss` vous devrez avoir repéré sur quel port tourne le service

```
port : 53
```

-   ouvrez ce port dans le firewall de la machine `dns-server.tp4.b1` (voir le mémo réseau Rocky)

```
[mathieu@dns-serveur etc]$ sudo firewall-cmd --add-port=53/tcp --permanent
success
[mathieu@dns-serveur etc]$ sudo firewall-cmd --reload
success
```

## [](#3-test)3. Test

🌞 **Sur la machine `node1.tp4.b1`**

-   configurez la machine pour qu'elle utilise votre serveur DNS quand elle a besoin de résoudre des noms

```
[mathieu@node1 network-scripts]$ cat /etc/resolv.conf
# Generated by NetworkManager
nameserver 10.4.1.201
```

-   assurez vous que vous pouvez :
    -   résoudre des noms comme `node1.tp4.b1` et `dns-server.tp4.b1`

```
[mathieu@node1 ~]$ dig node1.tp4.b1
; <<>> DiG 9.16.23-RH <<>> node1.tp4.b1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 46124
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1
;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 63d7d4e4ca6b2eb701000000636230bc3684a7d6d00f5f99 (good)
;; QUESTION SECTION:
;node1.tp4.b1.                  IN      A
;; ANSWER SECTION:
node1.tp4.b1.           86400   IN      A       10.4.1.11
;; Query time: 1 msec
;; SERVER: 10.4.1.201#53(10.4.1.201)
;; WHEN: Wed Nov 02 09:56:28 CET 2022
;; MSG SIZE  rcvd: 85
```

```
[mathieu@node1 ~]$ dig dns-server.tp4.b1
; <<>> DiG 9.16.23-RH <<>> dns-server.tp4.b1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 32167
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1
;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: e7688d1b5de248c6010000006362311c24f7fa06250e0082 (good)
;; QUESTION SECTION:
;dns-server.tp4.b1.             IN      A
;; ANSWER SECTION:
dns-server.tp4.b1.      86400   IN      A       10.4.1.201
;; Query time: 1 msec
;; SERVER: 10.4.1.201#53(10.4.1.201)
;; WHEN: Wed Nov 02 09:58:04 CET 2022
;; MSG SIZE  rcvd: 90
```

-   mais aussi des noms comme `www.google.com`

```
[mathieu@node1 ~]$ dig www.google.com
; <<>> DiG 9.16.23-RH <<>> www.google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 49839
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1
;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 0e42732e2fe376a501000000636230945fd22ee8a5d5c896 (good)
;; QUESTION SECTION:
;www.google.com.                        IN      A
;; ANSWER SECTION:
www.google.com.         300     IN      A       142.250.201.164
;; Query time: 461 msec
;; SERVER: 10.4.1.201#53(10.4.1.201)
;; WHEN: Wed Nov 02 09:55:48 CET 2022
;; MSG SIZE  rcvd: 87
```

🌞 **Sur votre PC**

-   utilisez une commande pour résoudre le nom `node1.tp4.b1` en utilisant `10.4.1.201` comme serveur DNS

```
PS C:\Windows\system32> Get-DnsClientServerAddress
InterfaceAlias               Interface Address ServerAddresses
                             Index     Family
--------------               --------- ------- ---------------
Wi-Fi                                40 IPv4    {192.168.0.254}
Wi-Fi                                40 IPv6    {}
```

```
PS C:\Windows\system32> netsh interface ipv4 add dnsserver "Wi-Fi" 10.4.1.201 index=7
```

```
PS C:\Windows\system32> ipconfig /all
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Realtek RTL8821CE 802.11ac PCIe Adapter
   Adresse physique . . . . . . . . . . . : 30-03-C8-C3-06-59
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6. . . . . . . . . . . . . .: 2a01:e0a:1dc:730:28a1:7006:b5c1:5f25(préféré)
   Adresse IPv6 temporaire . . . . . . . .: 2a01:e0a:1dc:730:e51c:980d:c2e9:decf(préféré)
   Adresse IPv6 de liaison locale. . . . .: fe80::28a1:7006:b5c1:5f25%40(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.0.34(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Bail obtenu. . . . . . . . . . . . . . : lundi 7 novembre 2022 19:25:58
   Bail expirant. . . . . . . . . . . . . : mardi 8 novembre 2022 10:02:26
   Passerelle par défaut. . . . . . . . . : fe80::e69e:12ff:fe8a:3862%40
                                       192.168.0.254
   Serveur DHCP . . . . . . . . . . . . . : 192.168.0.254
   IAID DHCPv6 . . . . . . . . . . . : 674235336
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2A-1F-A5-25-00-E0-4C-68-0E-1D
   Serveurs DNS. . .  . . . . . . . . . . : 192.168.0.254
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```

```
PS C:\Windows\system32> nslookup
Serveur par dÚfaut :   dns-server.tp4.b1
Address:  10.4.1.201
> node1.tp4.b1
Serveur :   dns-server.tp4.b1
Address:  10.4.1.201
Nom :    node1.tp4.b1
Address:  10.4.1.11
```

🦈 **Capture d'une requête DNS vers le nom `node1.tp4.b1` ainsi que la réponse**
