## 0. Prérequis

➜ Pour ce TP, on va se servir de VMs Rocky Linux. 1Go RAM c'est large large. Vous pouvez redescendre la mémoire vidéo aussi.

➜ Vous aurez besoin de deux réseaux host-only dans VirtualBox :

-   un premier réseau `10.3.1.0/24`
-   le second `10.3.2.0/24`
-   **vous devrez désactiver le DHCP de votre hyperviseur (VirtualBox) et définir les IPs de vos VMs de façon statique**

➜ Les firewalls de vos VMs doivent **toujours** être actifs (et donc correctement configurés).

➜ **Si vous voyez le p'tit pote 🦈 c'est qu'il y a un PCAP à produire et à mettre dans votre dépôt git de rendu.**

## [](#i-arp)I. ARP

Première partie simple, on va avoir besoin de 2 VMs.

Machine

`10.3.1.0/24`

`john`

`10.3.1.11`

`marcel`

`10.3.1.12`

```
   john               marcel
  ┌─────┐             ┌─────┐
  │     │    ┌───┐    │     │
  │     ├────┤ho1├────┤     │
  └─────┘    └───┘    └─────┘
```

> Référez-vous au [mémo Réseau Rocky](/it4lik/b1-reseau-2022/-/blob/main/cours/memo/rocky_network.md) pour connaître les commandes nécessaire à la réalisation de cette partie.

### [](#1-echange-arp)1. Echange ARP

🌞**Générer des requêtes ARP**
```
john
[mathieu@localhost ~]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=2.48 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=1.85 ms
64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=4.25 ms
64 bytes from 10.3.1.12: icmp_seq=4 ttl=64 time=2.45 ms
^C
--- 10.3.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3009ms
rtt min/avg/max/mdev = 1.847/2.758/4.253/0.901 ms
-------------------------------------------------------------
[mathieu@localhost ~]$ ip neigh show
10.3.1.1 dev enp0s8 lladdr 0a:00:27:00:00:33 REACHABLE
10.3.1.12 dev enp0s8 lladdr 08:00:27:ea:31:dc REACHABLE <----Mac
-------------------------------------------------------------
[mathieu@localhost ~]$ ip neigh show 10.3.1.12
10.3.1.12 dev enp0s8 lladdr 08:00:27:ea:31:dc STALE
```
```
marcel
[mathieu@localhost ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=1.35 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=0.558 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=64 time=1.04 ms
64 bytes from 10.3.1.11: icmp_seq=4 ttl=64 time=2.46 ms
^C
--- 10.3.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3051ms
rtt min/avg/max/mdev = 0.558/1.349/2.458/0.699 ms
-------------------------------------------------------------
[mathieu@localhost ~]$ ip neigh show
10.3.1.11 dev enp0s8 lladdr 08:00:27:48:b8:71 REACHABLE <---MAc
10.3.1.1 dev enp0s8 lladdr 0a:00:27:00:00:33 REACHABLE
-------------------------------------------------------------
[mathieu@localhost ~]$ ip a
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ea:31:dc brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.12/24 brd 10.3.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feea:31dc/64 scope link
       valid_lft forever preferred_lft forever
```

### [](#2-analyse-de-trames)2. Analyse de trames

🌞**Analyse de trames**
```
marcel
[mathieu@localhost ~]$ sudo tcpdump -i enp0s8 -c 10
dropped privs to tcpdump
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
15:51:11.227659 IP localhost.localdomain.ssh > 10.3.1.1.55794: Flags [P.], seq 2198492526:2198492586, ack 2642796424, win 295, length 60
15:51:11.228067 IP localhost.localdomain.ssh > 10.3.1.1.55794: Flags [P.], seq 60:248, ack 1, win 295, length 188
15:51:11.228309 IP 10.3.1.1.55794 > localhost.localdomain.ssh: Flags [.], ack 248, win 8191, length 0
15:51:11.228977 IP localhost.localdomain.ssh > 10.3.1.1.55794: Flags [P.], seq 248:644, ack 1, win 295, length 396
15:51:11.229686 IP localhost.localdomain.ssh > 10.3.1.1.55794: Flags [P.], seq 644:800, ack 1, win 295, length 156
15:51:11.230066 IP 10.3.1.1.55794 > localhost.localdomain.ssh: Flags [.], ack 800, win 8195, length 0
15:51:11.230268 IP localhost.localdomain.ssh > 10.3.1.1.55794: Flags [P.], seq 800:1060, ack 1, win 295, length 260
15:51:11.230943 IP localhost.localdomain.ssh > 10.3.1.1.55794: Flags [P.], seq 1060:1216, ack 1, win 295, length 156
15:51:11.231232 IP 10.3.1.1.55794 > localhost.localdomain.ssh: Flags [.], ack 1216, win 8193, length 0
15:51:11.231415 IP localhost.localdomain.ssh > 10.3.1.1.55794: Flags [P.], seq 1216:1372, ack 1, win 295, length 156
10 packets captured
11 packets received by filter
0 packets dropped by kernel
```
🦈 **Capture réseau `tp3_arp.pcapng`** qui contient un ARP request et un ARP reply

> **Si vous ne savez pas comment récupérer votre fichier `.pcapng`** sur votre hôte afin de l'ouvrir dans Wireshark, et me le livrer en rendu, demandez-moi.

## [](#ii-routage)II. Routage

Vous aurez besoin de 3 VMs pour cette partie. **Réutilisez les deux VMs précédentes.**

Machine

`10.3.1.0/24`

`10.3.2.0/24`

`router`

`10.3.1.254`

`10.3.2.254`

`john`

`10.3.1.11`

no

`marcel`

no

`10.3.2.12`

> Je les appelés `marcel` et `john` PASKON EN A MAR des noms nuls en réseau 🌻

```
   john                router              marcel
  ┌─────┐             ┌─────┐             ┌─────┐
  │     │    ┌───┐    │     │    ┌───┐    │     │
  │     ├────┤ho1├────┤     ├────┤ho2├────┤     │
  └─────┘    └───┘    └─────┘    └───┘    └─────┘
```

### [](#1-mise-en-place-du-routage)1. Mise en place du routage

🌞**Activer le routage sur le noeud `router`**
```
[mathieu@localhost ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: no
  masquerade: yes
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[mathieu@localhost ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s3 enp0s8
[mathieu@localhost ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[mathieu@localhost ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success

```

🌞**Ajouter les routes statiques nécessaires pour que `john` et `marcel` puissent se `ping`**
```
[mathieu@localhost ~]$ ip route add 10.3.1.11/24 via 10.3.2.254 dev enp0s8
[mathieu@localhost ~]$ ip route add 10.3.2.12/24 via 10.3.1.254 dev enp0s8

[mathieu@localhost ~]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=1.91 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=63 time=0.993 ms
64 bytes from 10.3.2.12: icmp_seq=3 ttl=63 time=1.25 ms

[mathieu@localhost ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=63 time=1.39 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=63 time=1.27 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=63 time=1.18 ms

```
[![THE SIZE](/it4lik/b1-reseau-2022/-/raw/main/tp/3/pics/thesize.png)](/it4lik/b1-reseau-2022/-/raw/main/tp/3/pics/thesize.png)

### [](#2-analyse-de-trames-1)2. Analyse de trames

🌞**Analyse des échanges ARP**

-   videz les tables ARP des trois noeuds
-   effectuez un `ping` de `john` vers `marcel`
    -   **le `tcpdump` doit être lancé sur la machine `john`**
-   essayez de déduire un les échanges ARP qui ont eu lieu
    -   en regardant la capture et/ou les tables ARP de tout le monde
-   répétez l'opération précédente (vider les tables, puis `ping`), en lançant `tcpdump` sur `marcel`
-   **écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, puis le ping et le pong, je veux TOUTES les trames** utiles pour l'échange

Par exemple (copiez-collez ce tableau ce sera le plus simple) :

ordre

type trame

IP source

MAC source

IP destination

MAC destination

1

Requête ARP

x

`marcel` `AA:BB:CC:DD:EE`

x

Broadcast `FF:FF:FF:FF:FF`

2

Réponse ARP

x

?

x

`marcel` `AA:BB:CC:DD:EE`

...

...

...

...

?

Ping

?

?

?

?

?

Pong

?

?

?

?

> Vous pourriez, par curiosité, lancer la capture sur `marcel` aussi, pour voir l'échange qu'il a effectué de son côté.

🦈 **Capture réseau `tp3_routage_marcel.pcapng`**

### [](#3-acc%C3%A8s-internet)3. Accès internet

🌞**Donnez un accès internet à vos machines**

-   ajoutez une carte NAT en 3ème inteface sur le `router` pour qu'il ait un accès internet
-   ajoutez une route par défaut à `john` et `marcel`
    -   vérifiez que vous avez accès internet avec un `ping`
    -   le `ping` doit être vers une IP, PAS un nom de domaine
-   donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser
    -   vérifiez que vous avez une résolution de noms qui fonctionne avec `dig`
    -   puis avec un `ping` vers un nom de domaine

🌞**Analyse de trames**

-   effectuez un `ping 8.8.8.8` depuis `john`
-   capturez le ping depuis `john` avec `tcpdump`
-   analysez un ping aller et le retour qui correspond et mettez dans un tableau :

ordre

type trame

IP source

MAC source

IP destination

MAC destination

1

ping

`marcel` `10.3.1.12`

`marcel` `AA:BB:CC:DD:EE`

`8.8.8.8`

?

2

pong

...

...

...

...

...

🦈 **Capture réseau `tp3_routage_internet.pcapng`**

## [](#iii-dhcp)III. DHCP

On reprend la config précédente, et on ajoutera à la fin de cette partie une 4ème machine pour effectuer des tests.

Machine

`10.3.1.0/24`

`10.3.2.0/24`

`router`

`10.3.1.254`

`10.3.2.254`

`john`

`10.3.1.11`

no

`bob`

oui mais pas d'IP statique

no

`marcel`

no

`10.3.2.12`

```
   john               router              marcel
  ┌─────┐             ┌─────┐             ┌─────┐
  │     │    ┌───┐    │     │    ┌───┐    │     │
  │     ├────┤ho1├────┤     ├────┤ho2├────┤     │
  └─────┘    └─┬─┘    └─────┘    └───┘    └─────┘
   dhcp        │
  ┌─────┐      │
  │     │      │
  │     ├──────┘
  └─────┘
```

### [](#1-mise-en-place-du-serveur-dhcp)1. Mise en place du serveur DHCP

🌞**Sur la machine `john`, vous installerez et configurerez un serveur DHCP** (go Google "rocky linux dhcp server").

-   installation du serveur sur `john`
-   créer une machine `bob`
-   faites lui récupérer une IP en DHCP à l'aide de votre serveur
    -   utilisez le mémo toujours, section "Définir une IP dynamique (DHCP)"

🌞**Améliorer la configuration du DHCP**

-   ajoutez de la configuration à votre DHCP pour qu'il donne aux clients, en plus de leur IP :
    -   une route par défaut
    -   un serveur DNS à utiliser
-   récupérez de nouveau une IP en DHCP sur `bob` pour tester :
    -   `bob` doit avoir une IP
        -   vérifier avec une commande qu'il a récupéré son IP
        -   vérifier qu'il peut `ping` sa passerelle
    -   il doit avoir une route par défaut
        -   vérifier la présence de la route avec une commande
        -   vérifier que la route fonctionne avec un `ping` vers une IP
    -   il doit connaître l'adresse d'un serveur DNS pour avoir de la résolution de noms
        -   vérifier avec la commande `dig` que ça fonctionne
        -   vérifier un `ping` vers un nom de domaine

### [](#2-analyse-de-trames-2)2. Analyse de trames

🌞**Analyse de trames**

-   lancer une capture à l'aide de `tcpdump` afin de capturer un échange DHCP
-   demander une nouvelle IP afin de générer un échange DHCP
-   exportez le fichier `.pcapng`
-   repérez, dans les trames DHCP observées dans Wireshark, les infos que votre serveur a fourni au client
    -   l'IP fournie au client
    -   l'adresse IP de la passerelle
    -   l'adresse du serveur DNS que vous proposez au client

🦈 **Capture réseau `tp3_dhcp.pcapng`**